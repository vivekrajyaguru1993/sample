import { Component, OnInit } from '@angular/core';
import {MatTabsModule} from '@angular/material/tabs';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { UploadsComponent } from '../uploads/uploads.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  columns = [1, 2, 3, 4, 5];
  arrTabs;
  arrOperations;

  constructor(private httpService: HttpClient, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.httpService.get('./assets/json/tabs.json').subscribe(
      data => {
        this.arrTabs = data;
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
    this.httpService.get('./assets/json/operations.json').subscribe(
      data => {
        this.arrOperations = data;
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }
  addMore() {
    const lastCount = this.columns[this.columns.length - 1];
    this.columns.push(lastCount + 1);
}

  openUploadDiaglog() {
    const dialogRef = this.dialog.open(UploadsComponent, {
      height: '400px',
      width: '500px',
      data: {data: 'Pass data if Needed'}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      const data = result;
    });
  }
}
