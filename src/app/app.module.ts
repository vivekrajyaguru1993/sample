import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CustomMaterialModule } from './core/material.module';
import { AppRoutingModule } from './core/app.routing.module';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './shared/auth.service';
import { AuthGuard } from './shared/auth.guard';
import { UploadsComponent } from './uploads/uploads.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { PriceDialogComponent } from './price-dialog/price-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    HeaderComponent,
    UploadsComponent,
    PriceDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/', pathMatch: 'full', canActivate: [AuthGuard]},
      { path: 'login', component: LoginComponent },
      { path: 'user', component: UserComponent, canActivate: [AuthGuard]}
    ]),
    MatDialogModule,
    MatFileUploadModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [UploadsComponent, PriceDialogComponent]
})
export class AppModule { }
