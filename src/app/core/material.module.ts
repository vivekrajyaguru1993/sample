import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {  MatTabsModule, MatCheckboxModule, MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule,
  MatToolbarModule, MatMenuModule, MatIconModule, MatProgressSpinnerModule, MatTooltipModule,
} from '@angular/material';
import { MatFileUploadModule } from 'angular-material-fileupload';
@NgModule({
  imports: [
  CommonModule,
  MatToolbarModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatTableModule,
  MatMenuModule,
  MatIconModule,
  MatTabsModule,
  MatProgressSpinnerModule,
  MatCheckboxModule,
  MatFileUploadModule,
  MatTooltipModule
  ],
  exports: [
  CommonModule,
   MatToolbarModule,
   MatButtonModule,
   MatCardModule,
   MatInputModule,
   MatDialogModule,
   MatTableModule,
   MatMenuModule,
   MatIconModule,
   MatTabsModule,
   MatProgressSpinnerModule,
   MatCheckboxModule,
   MatFileUploadModule,
   MatTooltipModule
   ],
})
export class CustomMaterialModule { }