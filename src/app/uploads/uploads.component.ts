import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PriceDialogComponent } from '../price-dialog/price-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-uploads',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.css']
})
export class UploadsComponent {
  fileSelectMsg = 'No file selected yet.';
  fileUploadMsg = 'No file uploaded yet.';
  disabled = false;

  constructor(public dialogRef: MatDialogRef<UploadsComponent>, @Inject(MAT_DIALOG_DATA) public data: any,  public dialog: MatDialog) {
    console.log('Data that pass from Parent Compoent', data);
  }

  openPriceDialog() {
      const dialogRef = this.dialog.open(PriceDialogComponent, {
        height: '250px',
        width: '300px',
        data: {data: 'Pass data if Needed'}
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        const data = result;
      });
  }
  close() {
    this.dialogRef.close();
  }


}
